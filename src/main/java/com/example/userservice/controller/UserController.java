package com.example.userservice.controller;

import com.example.userservice.model.User;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/user")
public class UserController {
    @RequestMapping(method = GET)
    public User test(@RequestHeader("AuthToken") String token) {
        return new User();
    }
}

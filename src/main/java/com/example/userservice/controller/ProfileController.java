package com.example.userservice.controller;

import com.example.userservice.model.Profile;
import com.example.userservice.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/user/profile")
public class ProfileController {
    @Autowired
    private ProfileRepository repository;

    @RequestMapping(method = GET)
    public List<Profile> findAll() { return repository.findAll(); }

    @RequestMapping(value = "/{id}", method = GET)
    public Profile findByUserId(@PathVariable("id") String id) {
        Optional<Profile> result = repository.findByUserId(id);

        try {
            if(result.isPresent()) {
                return result.get();
            }
            else{
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Sorry, the profile information couldn't be found.");
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Provide a correct id.");
        }
    }

    @RequestMapping(value="/{id}", method = PUT)
    public Profile update(@PathVariable("id") String id, @RequestBody Profile profileData){
        try {
            Optional<Profile> updatedProfile = repository.findByUserId(id).map(profile -> {
                profile.setName(profileData.getName());
                profile.setPictureUrl(profileData.getPictureUrl());

                return repository.save(profile);
            });

            if(updatedProfile.isPresent()) {
                return updatedProfile.get();
            }
            else{
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sorry, the profile data cannot be updated.");
            }
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Progress could not be updated", ex);
        }
    }
}
